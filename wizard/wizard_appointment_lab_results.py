# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder


__all__ = ['OpenAppointmentLabResults']

class OpenAppointmentLabResults(Wizard):
    'Open Appointment Lab Results'
    __name__ = 'wizard.gnuhealth.appointment.lab_results'

    start_state = 'appointment_lab_results'
    appointment_lab_results = StateAction('health_cameroun.act_app_lab_results')

    def do_appointment_lab_results(self, action):
        pool = Pool()
        LabResults = pool.get('gnuhealth.lab')

        appointment = Transaction().context.get('active_id')

        try:
            app_id = \
                Pool().get('gnuhealth.appointment').browse([appointment])[0]
        except:
            self.raise_user_error('no_record_selected')

        try:
            patient_id = app_id.patient.id
        except:
            self.raise_user_error('no_patient')

        labresults = LabResults.search(['patient','=',patient_id])

        action['pyson_domain'] = PYSONEncoder().encode([
            ('patient','=',app_id.patient.id),
            ])
        action['pyson_context'] = PYSONEncoder().encode({
            'patient': app_id.patient.id,
            })

        data = {'res_id': [x.id for x in labresults]}
        return action, data

    @classmethod
    def __setup__(cls):
        super(OpenAppointmentLabResults, cls).__setup__()
        cls._error_messages.update({
            'no_record_selected':
                'You need to select one Appointment record',
            'no_patient':
                'This appointment has no patient',
        })
