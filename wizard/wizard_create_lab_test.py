from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class RequestPatientLabTestStart(metaclass=PoolMeta):
    'Create Lab Test Report'
    __name__ = 'gnuhealth.patient.lab.test.request.start'

    @staticmethod
    def default_patient():
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')

        if Transaction().context.get('active_model') == 'gnuhealth.patient':
            return Transaction().context.get('active_id')
        if Transaction().context.get('active_model') == 'gnuhealth.appointment':
            appointment_id = Transaction().context.get('active_id')
            appointment = Appointment.search([('id','=',appointment_id)])[0]
            if appointment.patient:
                return appointment.patient.id
